package openxp.app.neo4jtool;

import com.enonic.xp.content.*;
import com.enonic.xp.context.Context;
import com.enonic.xp.context.ContextAccessor;
import com.enonic.xp.context.ContextBuilder;
import com.enonic.xp.data.PropertySet;
import com.enonic.xp.schema.content.ContentTypeName;
import com.enonic.xp.security.RoleKeys;
import com.enonic.xp.security.User;
import com.enonic.xp.security.auth.AuthenticationInfo;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import openxp.app.neo4jtool.executor.BoltCypherExecutor;
import openxp.app.neo4jtool.executor.CypherExecutor;
import openxp.app.neo4jtool.util.Iterators;
import openxp.app.neo4jtool.util.Util;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.Callable;

@Component(immediate = true)
public class Neo4jService {

    ContentService contentService;
    final Logger LOG = LoggerFactory.getLogger(Neo4jService.class);
    private Gson gson = new GsonBuilder().disableHtmlEscaping().create();
    private final CypherExecutor cypher;
    String uri = null;

    public Neo4jService() {
        if (uri == null) {
            uri = Util.DEFAULT_URL;
        }
        cypher = createCypherExecutor(uri);
    }

    @Activate
    public void initialize()
            throws Exception {

        LOG.info("initialize Neo4j Service");

        runAs(createInitContext(), () -> {
            //importAllTheThings();
            return null;
        });
    }

    private CypherExecutor createCypherExecutor(String uri) {
        try {
            String auth = new URL(uri.replace("bolt", "http")).getUserInfo();
            if (auth != null) {
                String[] parts = auth.split(":");
                return new BoltCypherExecutor(uri, parts[0], parts[1]);
            }
            return new BoltCypherExecutor(uri);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Invalid Neo4j-ServerURL " + uri);
        }
    }


    public Map<String, Object> productGraph(String query, int limit) {

        String cypherQuery = "MATCH (producer:Producer) -[:PRODUCES]->(product:Product)";
        if (query != "") {
            cypherQuery += " WHERE lower(product.title) CONTAINS {query} " +
                    " OR product.gtin STARTS WITH {query}" +
                    " OR lower(product.subtitle) CONTAINS {query}" +
                    " OR lower(product.ingredients) CONTAINS {query}" +
                    " OR lower(producer.title) CONTAINS {query}";
        }
        cypherQuery += " RETURN {title: producer.title, id: ID(producer)} as producer";
        cypherQuery += " ,collect({id: ID(product), gtin: product.gtin, title:product.title, " +
                " subtitle:product.subtitle, ingredients:product.ingredients," +
                " imageUrl: product.imageUrl}) as products";
        cypherQuery += " LIMIT {limit}";

        LOG.info("cypherQuery: {}", cypherQuery);
        Iterator<Map<String, Object>> result = cypher.query(cypherQuery, map("limit", limit, "query", query.toLowerCase()));

        List nodes = new ArrayList();
        List rels = new ArrayList();
        int i = 0;
        while (result.hasNext()) {

            Map<String, Object> row = result.next();

            nodes.add(map("data", row.get("producer"), "label", "producer"));
            int target = i;
            i++;

            for (Object product : (Collection) row.get("products")) {
                Map<String, Object> productMap = map("data", product, "label", "product");
                int source = nodes.indexOf(productMap);
                if (source == -1) {
                    nodes.add(productMap);
                    source = i++;
                }
                rels.add(map("source", source, "target", target));
            }
        }
        return map("nodes", nodes, "links", rels);
    }

    public void importAllTheThings() {
        LOG.info("IMPORT ALL THE THINGS");
        final ContentPath productPath = ContentPath.from("/innsikt/data/products/peiling-products");
        Content content = contentService.getByPath(productPath);

        ContentQuery contentQuery = ContentQuery.create()
                .addContentTypeName(ContentTypeName.from("org.informedindividual.innsikt:peiling-product"))
                /*.filterContentIds(ContentIds.from("6c456c5f-f047-4275-9b05-db14ff048bf2",
                        "5098a84f-0aca-4d8e-97c0-5d2c798093f7",
                        "700fe99f-ed1d-4f08-8aae-c3041583ed2c",
                        "2e41ca01-f90e-4b60-aedb-8bf5d048a967"))*/
                .from(0)
                .size(100000).build();

        FindContentIdsByQueryResult peilingProductsResult = contentService.find(contentQuery);
        LOG.info("found " + peilingProductsResult.getHits() + " peiling products");
        peilingProductsResult.getContentIds().forEach(contentId -> {
            Content c = contentService.getById(contentId);

            createProduct(c);
            createProducer(c.getData().getString("producer"), c.getData().getString("gtin"));

            c.getData().getStrings("allergens").forEach(allergen -> {
                createAllergen(allergen, c.getData().getString("gtin"));
            });

            createCategory(c.getData().getString("categoryImage"), c.getData().getString("gtin"));
            createMarks(c);
            createNutrients(c);
        });
    }

    public void createAllergen(String title, String gtin) {
        cypher.query(
                "MERGE (allergen:Allergen {title:{title}})",
                map("title", title, "gtin", gtin));

        cypher.query(
                "MATCH (allergen:Allergen {title: {title}}), (product: Product {gtin:{gtin}})" +
                        //" WHERE allergen.title = {title} AND product.gtin = {gtin}"+
                        " CREATE UNIQUE (product)-[r:CONTAINS_ALLERGEN]->(allergen)" +
                        "SET r.weight = coalesce(r.weight, 0) + 1",
                map("gtin", gtin, "title", title));

    }

    public void createMarks(Content c) {
        Iterable<PropertySet> marks = c.getData().getSets("marks");
        if (marks == null) {
            return;
        }
        String markUrlPrefix = "http://opdata.org/markimage/original/";
        marks.forEach(mark -> {
            String markImage = mark.getString("markImage");
            String markImageKey = mark.getString("markDownloadedImage");

            Content markDownloadedImage = contentService.getById(
                    ContentId.from(mark.getString("markDownloadedImage")));

            String markTitle = markDownloadedImage.getData().getString("caption");

            cypher.query(
                    "MERGE (productMark:ProductMark {title:{title}})" +
                            " ON MATCH set productMark.title={title},productMark.imageUrl={imageUrl}" +
                            " ON CREATE set productMark.title={title},productMark.imageUrl={imageUrl}",
                    map("title", markTitle, "imageUrl", markUrlPrefix + markImage));


            cypher.query(
                    "MATCH (product:Product {gtin:{gtin}}), " +
                            "(productMark:ProductMark {title:{title}}) " +
                            " CREATE UNIQUE (product)-[r:HAS_MARK]->(productMark)" +
                            "SET r.weight = coalesce(r.weight, 0) + 1",
                    map("gtin", c.getData().getString("gtin"), "title", markTitle));

        });
    }

    //TODO: Nutrients are not updated, just created first time
    public void createNutrients(Content c) {
        Iterable<PropertySet> nutrients = c.getData().getSets("details");
        if (nutrients == null) {
            return;
        }

        nutrients.forEach(nutrient -> {
            String title = nutrient.getString("title");
            String code = nutrient.getString("code");
            String amount = nutrient.getString("amount");
            String amountDef = nutrient.getString("amountDef");

            //Only for Energy in KJ
            String amount2 = nutrient.getString("amount2");
            String amountDef2 = nutrient.getString("amountDef2");
            if (!Strings.isNullOrEmpty(title)){
                cypher.query(
                        "MERGE (productNutrient:ProductNutrient {title:{title}})" +
                                " ON MATCH set productNutrient.title={title},productNutrient.code={code},productNutrient.amountDef={amountDef}" +
                                " ON CREATE set productNutrient.title={title},productNutrient.code={code},productNutrient.amountDef={amountDef},productNutrient.amountDef2={amountDef2}",
                        map("title", title, "code", code, "amountDef", amountDef, "amountDef2", amountDef2));
                //{amount:{amount},amount2:{amount2}}

                if (amount2 != null) {
                    cypher.query(
                            "MATCH (product:Product {gtin:{gtin}}), " +
                                    " (productNutrient:ProductNutrient {title:{title}}) " +
                                    " CREATE UNIQUE (product)-[r:NUTRIENT_AMOUNT {amount:{amount},amount2:{amount2}}]->(productNutrient)" +
                                    " SET r.weight = coalesce(r.weight, 0) + 1",
                            map("gtin", c.getData().getString("gtin"), "title", title, "amount", amount, "amount2", amount2));
                } else {

                    cypher.query(
                            "MATCH (product:Product {gtin:{gtin}}), " +
                                    " (productNutrient:ProductNutrient {title:{title}}) " +
                                    " CREATE UNIQUE (product)-[r:NUTRIENT_AMOUNT {amount:{amount}}]->(productNutrient)" +
                                    " SET r.weight = coalesce(r.weight, 0) + 1",
                            map("gtin", c.getData().getString("gtin"), "title", title, "amount", amount));
                }
            }
        });
    }

    public void createCategory(String categoryImage, String gtin) {
        if (Strings.isNullOrEmpty(categoryImage)){
            return;
        }
        String title = categoryImage.substring(categoryImage.indexOf('_') + 1, categoryImage.indexOf('.'));
        title = title.replaceAll("_", " ");
        title = title.substring(0, 1).toUpperCase() + title.substring(1);
        title = title.replace("Oster", "Cheeses");
        categoryImage = categoryImage.replace("_oster", "_cheeses");
        String imageUrl = "http://opdata.org/productcategoryimage/original/" + categoryImage;
        //LOG.info("Create category {}",title);
        cypher.query(
                "MERGE (productCategory:ProductCategory {title:{title}})" +
                        " ON CREATE set productCategory.imageUrl={imageUrl}",
                map("title", title, "imageUrl", imageUrl));

        cypher.query(
                "MATCH (productCategory:ProductCategory {title:{title}}), " +
                        "(product:Product {gtin:{gtin}}) " +
                        "CREATE UNIQUE (product)-[r:IN_CATEGORY]->(productCategory)" +
                        "SET r.weight = coalesce(r.weight, 0) + 1",
                map("gtin", gtin, "title", title));
    }


    public void createProduct(Content c) {
        String title = c.getDisplayName();
        String subtitle = c.getData().getString("subtitle");
        String gtin = c.getData().getString("gtin");
        String imageUrl = "";
        if (c.getData().getString("downloadedImage")!=null){
            imageUrl = "http://opdata.org/productimage/original/" + gtin + ".png";
        }
        String ingredients = c.getData().getString("ingredients");
        cypher.query(
                "MERGE (product:Product {gtin:{gtin}})" +
                        " ON MATCH set product.title={title},product.subtitle={subtitle},product.imageUrl={imageUrl}, product.ingredients={ingredients}" +
                        " ON CREATE set product.title={title},product.subtitle={subtitle},product.imageUrl={imageUrl}, product.ingredients={ingredients}",
                map("title", title, "subtitle", subtitle, "gtin", gtin, "imageUrl", imageUrl, "ingredients", ingredients));
    }

    public void createProducer(String displayName, String gtin) {
        cypher.query(
                "MERGE (producer:Producer {title:{title}})",
                map("gtin", gtin, "title", displayName));

        cypher.query(
                "MATCH (producer:Producer {title:{title}}), " +
                        "(product:Product {gtin:{gtin}}) " +
                        "CREATE UNIQUE (product)-[r:PRODUCED_BY]->(producer)" +
                        "SET r.weight = coalesce(r.weight, 0) + 1",
                map("gtin", gtin, "title", displayName));

        cypher.query(
                "MATCH (producer:Producer {title:{title}}), " +
                        "(product:Product {gtin:{gtin}}) " +
                        "CREATE UNIQUE (producer)-[r:PRODUCES]->(product)" +
                        "SET r.weight = coalesce(r.weight, 0) + 1",
                map("gtin", gtin, "title", displayName));
    }


    /*@SuppressWarnings("unchecked")
    public Iterable<Map<String,Object>> search(String query) {
        if (query==null || query.trim().isEmpty()) return Collections.emptyList();

        return Iterators.asCollection(cypher.query(
                "MATCH (movie:Movie)\n" +
                        " WHERE lower(movie.title) CONTAINS {part}\n" +
                        " RETURN movie",
                map("part", query.toLowerCase())));
    }*/

    @SuppressWarnings("unchecked")
    public Iterable<Map<String, Object>> search(String query) {
        if (query == null || query.trim().isEmpty()) return Collections.emptyList();

        return Iterators.asCollection(cypher.query(
                "MATCH (product:Product)\n" +
                        " WHERE lower(product.title) CONTAINS {part}" +
                        " RETURN product",
                map("part", query.toLowerCase())));
    }


    /**
     * A short-hand method for creating a {@link Map} of key/value pairs where
     * keys are {@link String}s and values are {@link Object}s.
     *
     * @param objects alternating key and value.
     * @return a Map with the entries supplied by {@code objects}.
     */
    public static Map<String, Object> map(Object... objects) {
        return genericMap(objects);
    }

    /**
     * A short-hand method for creating a {@link Map} of key/value pairs.
     *
     * @param objects alternating key and value.
     * @param <K>     type of keys
     * @param <V>     type of values
     * @return a Map with the entries supplied by {@code objects}.
     */
    public static <K, V> Map<K, V> genericMap(Object... objects) {
        return genericMap(new HashMap<K, V>(), objects);
    }

    /**
     * A short-hand method for adding key/value pairs into a {@link Map}.
     *
     * @param targetMap the {@link Map} to put the objects into.
     * @param objects   alternating key and value.
     * @param <K>       type of keys
     * @param <V>       type of values
     * @return a Map with the entries supplied by {@code objects}.
     */
    @SuppressWarnings("unchecked")
    public static <K, V> Map<K, V> genericMap(Map<K, V> targetMap, Object... objects) {
        int i = 0;
        while (i < objects.length) {
            targetMap.put((K) objects[i++], (V) objects[i++]);
        }
        return targetMap;
    }

    private <T> T runAs(final Context context, final Callable<T> runnable) {
        return context.callWith(runnable);
    }

    @org.osgi.service.component.annotations.Reference
    public void setContentService(final ContentService contentService) {
        this.contentService = contentService;
    }

    private Context createInitContext() {
        return ContextBuilder.from(ContextAccessor.current()).
                authInfo(AuthenticationInfo.create().principals(RoleKeys.CONTENT_MANAGER_ADMIN).user(User.ANONYMOUS).build()).
                branch(ContentConstants.BRANCH_DRAFT).
                repositoryId(ContentConstants.CONTENT_REPO.getId()).
                build();
    }
}
