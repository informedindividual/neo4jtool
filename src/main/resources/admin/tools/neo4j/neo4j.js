var portalLib = require('/lib/xp/portal');
var mustacheLib = require('/lib/xp/mustache');
var thymeleafLib = require('/lib/xp/thymeleaf');
var xpVersion = require('/lib/status').getXpVersion();

exports.get = function (req) {
    var view = resolve('neo4jd3.html');

    // Variables required by the Launcher Panel
    var adminUrl = portalLib.url({path: '/admin'});
    var adminAssetsUrl = portalLib.url({path: "/admin/assets/" + xpVersion});
    var assetsUrl = portalLib.assetUrl({path: ""});
    var serviceUrl = portalLib.serviceUrl({service:'neo4j'});
    var params = {
        adminUrl: adminUrl,
        adminAssetsUrl: adminAssetsUrl,
        assetsUrl: assetsUrl,
        serviceUrl: serviceUrl
    };

    return {
        body: thymeleafLib.render(view, params)
    };
};
