var portalLib = require('/lib/xp/portal');
var contentLib = require('/lib/xp/content');
var thymeleafLib = require('/lib/xp/thymeleaf');

exports.get = function (req) {
    log.info(req);
    var contentId = req.params.contentId;

    var view = resolve('neo4j.html');
    var serviceUrl = portalLib.serviceUrl({service:'neo4j'});
    var content = contentLib.get({key:contentId});

    var params = {
        serviceUrl: serviceUrl,
        createProductUrl: serviceUrl+createProductUrl(content),
        createProducerUrl: serviceUrl+createProducerUrl(content),
        content: content,
        productTitle:content.displayName,
        productSubtitle:content.data.subtitle,
        producer:content.data.producer
    };

    log.info("params %s",params);

    function createProductUrl(content){
        return '?createProduct=true&title='+content.displayName +
            "&subtitle="+content.data.subtitle +
            "&gtin="+content.data.gtin;
    }

    function createProducerUrl(content){
        return '?createProducer=true&title='+content.data.producer+"&gtin="+content.data.gtin;
    }

    return {
        body: thymeleafLib.render(view, params),
        contentType: 'text/html'
    };
};