var neo4j = require('/lib/neo4j');
var portalLib = require('/lib/xp/portal');

exports.get = function (req) {

    var result = {};

    if (req.params.search !== undefined) {
        var query = "";
        if (req.params.q !== undefined) {
            query = req.params.q;
        }

        result = neo4j.search(query);
        result.forEach(function (r) {

        });

        //var stringResult = JSON.stringify(result.toString());
        //log.info("stringResult %s", stringResult);
        //result = stringResult.split('=').join(':');
        //log.info("splitResult %s", result);
    } else if (req.params.graph !== undefined) {
        var limit = 50;
        if (req.params.limit !== undefined) {
            limit = req.params.limit;
        }
        result = neo4j.graph(limit);

    } else if (req.params.productGraph !== undefined) {
        var limit = 50;
        var q = '';
        if (req.params.limit !== undefined) {
            limit = req.params.limit;
        }
        if (req.params.q !== undefined) {
            q = req.params.q;
        }
        result = neo4j.productGraph(q, limit);
        //addProductImages(result);

    } else if (req.params.createProduct !== undefined) {
        if (req.params.title !== undefined && req.params.gtin !== undefined) {
            result = neo4j.createProduct(req.params.title, req.params.subtitle, req.params.gtin);
        }

    } else if (req.params.createProducer !== undefined) {
        if (req.params.displayName !== undefined && req.params.gtin !== undefined) {
            result = neo4j.createProducer(req.params.displayName, req.params.gtin);
        }
    }

    return {
        body: result,
        status: 200,
        contentType: 'application/json'
    };

    /*function addProductImages(result) {
        if (result.nodes) {
            result.nodes.forEach(function (node) {
                log.info("Resolve image for " + node.label + " " + node.data.title);
                if (node.label === 'product' && node.data.gtin !== undefined) {
                    try {
                        node.data.productImage = portalLib.assetUrl({
                            path: 'icons/noimage.png',
                            type: 'absolute'
                        });
                    }catch (e){
                        log.info("exception " + e);
                    }
                    log.info(node.data.productImage);
                }

            })
        }
    }*/

};

exports.post = function (req) {
    return exports.get(req);
}