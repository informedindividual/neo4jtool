var neo4jService = __.newBean("openxp.app.neo4jtool.Neo4jService");
var utilLib = require('/lib/enonic/util');

exports.graph = function (limit) {
  return neo4jService.graph(limit);
};

exports.productGraph = function (q, limit) {
    return neo4jService.productGraph(q, limit);
};

exports.search = function (query) {
    return neo4jService.search(query);
};

exports.createProduct = function (displayName, subtitle, gtin) {
    return neo4jService.createProduct(displayName, subtitle, gtin);
};

exports.createProducer = function (displayName, gtin) {
    return neo4jService.createProducer(displayName, gtin);
};